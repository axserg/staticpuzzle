<?php

$errorMSG = "";

// NAME
if (empty($_POST["name"])) {
    $errorMSG = "Ingresa tu nombre ";
} else {
    $name = $_POST["name"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Ingresa tu email ";
} else {
    $email = $_POST["email"];
}

// MSG SUBJECT+
if (empty($_POST["msg_subject"])) {
    $errorMSG .= "Ingresa un mensaje ";
} else {
    $msg_subject = $_POST["msg_subject"];
}


// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "Ingresa un mensaje ";
} else {
    $message = $_POST["message"];
}


$EmailTo = "info@puzzleinformatica.mx";
$Subject = "Mensaje para puzzle";

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Subject: ";
$Body .= $msg_subject;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $message;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);
echo "¡ Gracias! tu mensaje ha sido enviado exitosamente.";
// redirect to success page
if ($success && $errorMSG == ""){
   echo "";
}else{
    if($errorMSG == ""){
        echo "Algo salió mal";
    } else {
        echo $errorMSG;
    }
}

?>